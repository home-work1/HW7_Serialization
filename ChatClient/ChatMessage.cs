﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatClient
{
    public class ChatMessage
    {
        private DateTime _sendTime;

        private string _message;

        private string _exceptionMessage;

        public DateTime SendTime => _sendTime;

        public string Message => _message;

        public string ExceptionMessage => _exceptionMessage; 

        public void SetMessage(string mess)
        {
            _message = mess; 
        }

        public void SetSendTime(DateTime time)
        {
            _sendTime = time;
        }

        public void SetExceptionMessage(string excMes)
        {
            _exceptionMessage = excMes;
        }

    }
}
