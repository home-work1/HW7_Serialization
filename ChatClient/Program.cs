﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;

namespace ChatClient
{
    class Program
    {
        static string userName;
        private const string host = "127.0.0.1";
        private const int port = 8888;
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                client.Connect(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток

                string message = userName;
                byte[] data = Encoding.Unicode.GetBytes(message);
                stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start(); //старт потока
                Console.WriteLine("Добро пожаловать, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
        // отправка сообщений
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            ChatMessage chatMessage = new ChatMessage();

            while (true)
            {
                string message = Console.ReadLine();

                try
                {
                    chatMessage.SetExceptionMessage(null);
                    chatMessage.SetMessage(message);
                    chatMessage.SetSendTime(DateTime.Now);
                    if ((new Random()).NextDouble() > 0.5)
                    {
                        throw new Exception("Ошибка случая");
                    }
                }
                catch (Exception ex)
                {
                    chatMessage.SetMessage(null);
                    chatMessage.SetExceptionMessage(ex.Message);
                }

                string output = JsonConvert.SerializeObject(chatMessage);


                byte[] data = Encoding.Unicode.GetBytes(output);
                stream.Write(data, 0, data.Length);
            }
        }
        // получение сообщений
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string mesSerialize = builder.ToString();

                    ChatMessage mess  = JsonConvert.DeserializeObject<ChatMessage>(mesSerialize);

                    if(mess.ExceptionMessage == null)
                    {
                        Console.WriteLine($"Сообщение от {mess.SendTime} : {mess.Message}");
                    }
                    else
                    {
                        Console.WriteLine($"Исключение возникшее у отправителя {mess.SendTime} : {mess.ExceptionMessage}");
                    }


                }
                catch(Exception exc)
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.WriteLine($"Возникло исключение: {exc.Message}");
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}